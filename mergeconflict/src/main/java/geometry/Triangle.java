package geometry;

public class Triangle {
    private int baseSize;
    private int heightSize;

    public Triangle(int baseSize, int heightSize){
        this.baseSize = baseSize;
        this.heightSize = heightSize;
    }
    public int getBase(){
        return this.baseSize;
    }
    public int getHeight(){
        return this.heightSize;
    }
    public int getArea(){
        int area = (this.baseSize * this.heightSize)/2;
        return area;
    }
    public String toString(){
        String toReturn = "Base: "+this.baseSize+", Height: "+this.heightSize;
        return toReturn;
    }

}
