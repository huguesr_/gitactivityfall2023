package geometry;

public class Square {
     private int sideSize;

    public Square(int sideSize) {
        this.sideSize = sideSize;
    }

     public int getSideSize(){
        return this.sideSize;
     }

     public int getArea(){
        int area = this.sideSize * this.sideSize;
        return area;
     }

     @Override
    public String toString() {
        return "Square [ sideSize = " + sideSize + " ]";
    }
}
