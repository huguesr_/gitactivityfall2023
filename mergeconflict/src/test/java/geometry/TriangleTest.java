package geometry;
import static org.junit.Assert.*;
import org.junit.Test;
public class TriangleTest {
    
    @Test
    public void getBaseTest(){
        Triangle test = new Triangle(3, 4);
        assertEquals(3, test.getBase());
    }
    @Test
    public void getHeightTest(){
        Triangle test = new Triangle(3, 4);
        assertEquals(4, test.getHeight());
    }
    @Test
    public void getAreaTest(){
        Triangle test = new Triangle(3, 4);
        assertEquals(6, test.getArea());
    }
    @Test
    public void toStringTest(){
        Triangle test = new Triangle(3, 4);
        assertEquals("Base: 3, Height: 4", test.toString());
    }
}
