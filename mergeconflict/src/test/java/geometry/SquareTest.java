package geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SquareTest {
    
    @Test
    public void getSideSizeTest(){
        Square sq = new Square(2);
        assertEquals(2, sq.getSideSize());
    }

    @Test
    public void getAreaTest(){
        Square sq = new Square(3);
        assertEquals(9, sq.getArea());
    }

    @Test
    public void toStringTest(){
        Square sq = new Square(4);
        assertEquals("Square [ sideSize = 4 ]", sq.toString());
    }
}
